package net.cubekrowd.cubereport;

public class Permissions {
    public static String REPORT_USE = "cubereport.report.use";
    public static String REPORT_SEE = "cubereport.report.see";
    public static String HELPOP_USE = "cubereport.helpop.use";
    public static String HELPOP_SEE = "cubereport.helpop.see";
}
