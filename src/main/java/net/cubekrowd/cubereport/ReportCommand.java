/*

    cubereport
    Copyright (C) 2017  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.cubereport;

import club.minnced.discord.webhook.send.WebhookEmbed;
import club.minnced.discord.webhook.send.WebhookEmbedBuilder;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

public class ReportCommand extends Command implements TabExecutor {

    private final CubeReportPlugin plugin;

    public ReportCommand(final CubeReportPlugin plugin) {
        super("report", Permissions.REPORT_USE);
        this.plugin = plugin;
    }

    @Override
    public void execute(final CommandSender sender, final String[] args) {
        List<String> toggledUUIDS = plugin.getData().getStringList("toggled");

        // Check if the player provided enough arguments.
        if (args.length == 1 && args[0].equalsIgnoreCase("toggle") && sender.hasPermission(Permissions.REPORT_SEE)) {
            // TODO(traks): modifying data should be made thread safe
            if (sender instanceof ProxiedPlayer player) {
                var senderUUIDS = player.getUniqueId().toString();
                if (toggledUUIDS.contains(senderUUIDS)) {
                    toggledUUIDS.remove(senderUUIDS);
                    sender.sendMessage(new ComponentBuilder("You will now see report messages again.")
                            .color(ChatColor.GREEN).create());
                } else {
                    toggledUUIDS.add(senderUUIDS);
                    sender.sendMessage(new ComponentBuilder("You will no longer see report messages.")
                            .color(ChatColor.GREEN).create());
                }
                plugin.getData().set("toggled", toggledUUIDS);
            } else {
                sender.sendMessage(new ComponentBuilder("Error: Only players can toggle reports")
                        .color(ChatColor.RED).create());
            }
            return;
        } else if (args.length < 2) {
            sender.sendMessage(new ComponentBuilder("Usage: /report <player> <reason>").color(ChatColor.RED).create());
            return;
        }

        // NOTE(traks): check command cooldown
        int cooldownSecs = plugin.getAndUpdateRemainingCooldownSecs(sender);
        if (cooldownSecs > 0) {
            sender.sendMessage(new ComponentBuilder("You can use the report function in ").color(ChatColor.RED)
                    .append("" + cooldownSecs).color(ChatColor.GRAY).append(cooldownSecs > 1 ? " seconds!" : " second!")
                    .color(ChatColor.RED).create());
            return;
        }

        // Join the arguments from 2 to N with a blankspace.
        var message = Stream.of(args).skip(1).collect(Collectors.joining(" "));

        // Check if the reported player is online.
        // List of everyone online
        final Collection<String> usernames = plugin.getProxy().getPlayers().stream().map(pi -> pi.getName())
                .collect(Collectors.toList());
        final String targetName = usernames.stream().filter(u -> u.equalsIgnoreCase(args[0])).findFirst().orElse(null);
        if (targetName == null) {
            sender.sendMessage(new ComponentBuilder("Error: Player '" + args[0] + "' is not online.")
                    .color(ChatColor.RED).create());
            return;
        }

        final UUID targetUUID = plugin.getProxy().getPlayer(targetName).getUniqueId();
        final ServerInfo targetServer = plugin.getProxy().getPlayer(targetUUID).getServer().getInfo();
        var senderName = sender instanceof ProxiedPlayer ? sender.getName() : "Console";

        // Send the "message was submitted" message to the reporter
        // &cYou have issued a report against &7{0} &cfor: &7{1}
        final BaseComponent[] msgSubmitted = new ComponentBuilder("You have issued a report against ")
                .color(ChatColor.RED).append(targetName).color(ChatColor.GRAY).append(" for: ").color(ChatColor.RED)
                .append(message).color(ChatColor.GRAY).create();
        sender.sendMessage(msgSubmitted);

        // Send the message to all players who have the see permission
        final BaseComponent[] msgReported = new ComponentBuilder(senderName).color(ChatColor.GRAY)
                .append(" has reported ").color(ChatColor.RED).append(targetName).color(ChatColor.GRAY).append(" for ")
                .color(ChatColor.RED).append(message).color(ChatColor.GRAY).append(" Server: ").color(ChatColor.RED)
                .append(targetServer.getName()).color(ChatColor.GRAY).create();

        plugin.getProxy().getPlayers().stream().filter(p -> p.hasPermission(Permissions.REPORT_SEE))
                .filter(p -> !toggledUUIDS.contains(p.getUniqueId().toString()))
                .forEach(p -> p.sendMessage(msgReported));

        // NOTE(traks): send to Discord via webhook
        var client = plugin.getDiscordClient();
        if (client != null) {
            var builder = new WebhookEmbedBuilder()
                    .setAuthor(new WebhookEmbed.EmbedAuthor("CubeReport", null, null))
                    .setDescription(String.format(":bell: **%s** has reported **%s** on server **%s** for: *%s*",
                            CubeReportPlugin.escapeDiscordEmbedText(senderName),
                            CubeReportPlugin.escapeDiscordEmbedText(targetName),
                            CubeReportPlugin.escapeDiscordEmbedText(targetServer.getName()),
                            CubeReportPlugin.escapeDiscordEmbedText(message)))
                    .setColor(0x01a9f4)
                    .setTimestamp(Instant.now());
            client.send(builder.build());
        }
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender sender, String[] args) {
        if (args.length == 1) {
            var completions = new ArrayList<String>();
            var prefix = args[0].toLowerCase(Locale.ENGLISH);
            for (var p : ProxyServer.getInstance().getPlayers()) {
                if (p.getName().toLowerCase(Locale.ENGLISH).startsWith(prefix)) {
                    completions.add(p.getName());
                }
            }
            if (sender.hasPermission(Permissions.REPORT_SEE) && "toggle".startsWith(prefix)) {
                completions.add("toggle");
            }
            return completions;
        }
        return Collections.emptyList();
    }
}
