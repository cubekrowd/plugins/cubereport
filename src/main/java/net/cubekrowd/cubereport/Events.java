/*

    cubereport
    Copyright (C) 2017  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.cubereport;

import club.minnced.discord.webhook.send.WebhookEmbed;
import club.minnced.discord.webhook.send.WebhookEmbedBuilder;
import java.time.Instant;
import java.util.Locale;
import me.confuser.banmanager.bungee.api.events.NameBanEvent;
import me.confuser.banmanager.bungee.api.events.NameUnbanEvent;
import me.confuser.banmanager.bungee.api.events.PlayerBanEvent;
import me.confuser.banmanager.bungee.api.events.PlayerKickedEvent;
import me.confuser.banmanager.bungee.api.events.PlayerMuteEvent;
import me.confuser.banmanager.bungee.api.events.PlayerNoteCreatedEvent;
import me.confuser.banmanager.bungee.api.events.PlayerUnbanEvent;
import me.confuser.banmanager.bungee.api.events.PlayerUnmuteEvent;
import me.confuser.banmanager.bungee.api.events.PlayerWarnEvent;
import me.confuser.banmanager.common.data.PlayerData;
import me.confuser.banmanager.common.util.DateUtils;
import me.confuser.banmanager.common.util.Message;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class Events implements Listener {

    private final CubeReportPlugin plugin;

    public Events(final CubeReportPlugin plugin) {
        super();
        this.plugin = plugin;
    }

    public static String format(String format, Object... args) {
        return String.format(Locale.ENGLISH, format, args);
    }

    public static class Builder extends WebhookEmbedBuilder {
        private final long eventTime;

        public Builder(String msg, long eventTime) {
            setAuthor(new WebhookEmbed.EmbedAuthor("BanManager", null, null));
            setDescription(msg);
            setColor(0xd97708);
            setTimestamp(Instant.ofEpochSecond(eventTime));
            this.eventTime = eventTime;
        }

        public Builder showExpiration(long expires) {
            var expiration = expires != 0 ? format("<t:%s>", expires) : "Never";
            var duration = expires != 0 ? DateUtils.formatDifference(expires - eventTime) : "Permanent";
            addField(new WebhookEmbed.EmbedField(true, "Expires", expiration));
            addField(new WebhookEmbed.EmbedField(true, "Duration", duration));
            return this;
        }

        public Builder showCreation(long created) {
            var creation = format("<t:%s>", created);
            var duration = DateUtils.formatDifference(eventTime - created);
            addField(new WebhookEmbed.EmbedField(true, "Created", creation));
            addField(new WebhookEmbed.EmbedField(true, "Duration", duration));
            return this;
        }
    }

    public String name(String name) {
        return format("**%s**", CubeReportPlugin.escapeDiscordEmbedText(name));
    }

    public String name(PlayerData player) {
        return name(player.getName());
    }

    public String nameLink(PlayerData player) {
        var websiteMsg = Message.get("info.website.player")
                .set("player", player.getName())
                .set("uuid", player.getUUID().toString())
                .set("playerId", player.getUUID().toString())
                .toString();
        return format("[%s](%s)", name(player), websiteMsg);
    }

    public String reason(String reason) {
        return format("*%s*", CubeReportPlugin.escapeDiscordEmbedText(reason));
    }

    @EventHandler
    public void onBanEvent(PlayerBanEvent e) {
        var p = e.getBan();
        var msg = format(":hammer: %s has banned %s for: %s", name(p.getActor()), nameLink(p.getPlayer()), reason(p.getReason()));
        sendDiscordAlert(new Builder(msg, p.getCreated()).showExpiration(p.getExpires()));
    }

    @EventHandler
    public void onNameBanEvent(NameBanEvent e) {
        var p = e.getBan();
        var msg = format(":face_with_symbols_over_mouth: %s has banned the name %s for: %s", name(p.getActor()), name(p.getName()), reason(p.getReason()));
        sendDiscordAlert(new Builder(msg, p.getCreated()).showExpiration(p.getExpires()));
    }

    @EventHandler
    public void onMuteEvent(PlayerMuteEvent e) {
        var p = e.getMute();
        var msg = format(":mute: %s has muted %s for: %s", name(p.getActor()), nameLink(p.getPlayer()), reason(p.getReason()));
        sendDiscordAlert(new Builder(msg, p.getCreated()).showExpiration(p.getExpires()));
    }

    @EventHandler
    public void onKickEvent(PlayerKickedEvent e) {
        var p = e.getKick();
        var msg = format(":boot: %s has kicked %s for: %s", name(p.getActor()), nameLink(p.getPlayer()), reason(p.getReason()));
        sendDiscordAlert(new Builder(msg, p.getCreated()));
    }

    @EventHandler
    public void onWarnEvent(PlayerWarnEvent e) {
        var p = e.getWarning();
        var msg = format(":warning: %s has warned %s for: %s", name(p.getActor()), nameLink(p.getPlayer()), reason(p.getReason()));
        sendDiscordAlert(new Builder(msg, p.getCreated()));
    }

    @EventHandler
    public void onNoteEvent(PlayerNoteCreatedEvent e) {
        var p = e.getNote();
        var msg = format(":scroll: %s has created a note for %s that says: %s", name(p.getActor()), nameLink(p.getPlayer()), reason(p.getMessage()));
        sendDiscordAlert(new Builder(msg, p.getCreated()));
    }

    @EventHandler
    public void onUnbanEvent(PlayerUnbanEvent e) {
        var p = e.getBan();
        if (p.hasExpired()) {
            var msg = format(":unlock: %s has been unbanned. Ban reason: %s", nameLink(p.getPlayer()), reason(p.getReason()));
            sendDiscordAlert(new Builder(msg, p.getExpires()).showCreation(p.getCreated()));
        } else {
            var msg = format(":unlock: %s has unbanned %s. Ban reason: %s", name(e.getActor()), nameLink(p.getPlayer()), reason(p.getReason()));
            sendDiscordAlert(new Builder(msg, System.currentTimeMillis() / 1000).showCreation(p.getCreated()));
        }
    }

    @EventHandler
    public void onNameUnbanEvent(NameUnbanEvent e) {
        var p = e.getBan();
        if (p.hasExpired()) {
            var msg = format(":unlock: The name %s has been unbanned. Ban reason: %s", name(p.getName()), reason(p.getReason()));
            sendDiscordAlert(new Builder(msg, p.getExpires()).showCreation(p.getCreated()));
        } else {
            var msg = format(":unlock: %s has unbanned the name %s. Ban reason: %s", name(e.getActor()), name(p.getName()), reason(p.getReason()));
            sendDiscordAlert(new Builder(msg, System.currentTimeMillis() / 1000).showCreation(p.getCreated()));
        }
    }

    @EventHandler
    public void onUnmuteEvent(PlayerUnmuteEvent e) {
        var p = e.getMute();
        if (p.hasExpired()) {
            var msg = format(":unlock: %s has been unmuted. Mute reason: %s", nameLink(p.getPlayer()), reason(p.getReason()));
            sendDiscordAlert(new Builder(msg, p.getExpires()).showCreation(p.getCreated()));
        } else {
            var msg = format(":unlock: %s has unmuted %s. Mute reason: %s", name(e.getActor()), nameLink(p.getPlayer()), reason(p.getReason()));
            sendDiscordAlert(new Builder(msg, System.currentTimeMillis() / 1000).showCreation(p.getCreated()));
        }
    }

    public void sendDiscordAlert(WebhookEmbedBuilder builder) {
        var client = plugin.getDiscordClient();
        if (client != null) {
            client.send(builder.build());
        }
    }
}
