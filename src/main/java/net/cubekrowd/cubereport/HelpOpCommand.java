package net.cubekrowd.cubereport;

import club.minnced.discord.webhook.send.WebhookEmbed;
import club.minnced.discord.webhook.send.WebhookEmbedBuilder;
import java.time.Instant;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class HelpOpCommand extends Command {
    private final CubeReportPlugin plugin;

    public HelpOpCommand(CubeReportPlugin plugin) {
        super("helpop", Permissions.HELPOP_USE, "helpop", "eac", "amsg", "eamsg", "ehelpop");
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length < 1) {
            sender.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "Usage: /helpop <message>"));
            return;
        }

        // NOTE(traks): check command cooldown
        int cooldownSecs = plugin.getAndUpdateRemainingCooldownSecs(sender);
        if (cooldownSecs > 0) {
            sender.sendMessage(new ComponentBuilder("You can use helpop in ").color(ChatColor.RED)
                    .append("" + cooldownSecs).color(ChatColor.GRAY).append(cooldownSecs > 1 ? " seconds!" : " second!")
                    .color(ChatColor.RED).create());
            return;
        }

        var senderName = sender instanceof ProxiedPlayer ? sender.getName() : "Console";
        var inputMessage = Stream.of(args).collect(Collectors.joining(" "));
        String format = ChatColor.DARK_GRAY + "[" + ChatColor.DARK_AQUA + "Help" + ChatColor.GOLD + "Op" + ChatColor.DARK_GRAY + "] " + ChatColor.BLUE + senderName + " " + ChatColor.WHITE + inputMessage;
        BaseComponent[] msg = TextComponent.fromLegacyText(format);

        plugin.getProxy().getPlayers().stream()
                .filter(pp -> pp.hasPermission(Permissions.HELPOP_SEE) || pp == sender)
                .forEach(pp -> pp.sendMessage(msg));

        // NOTE(traks): send to Discord via webhook
        var client = plugin.getDiscordClient();
        if (client != null) {
            var builder = new WebhookEmbedBuilder()
                    .setAuthor(new WebhookEmbed.EmbedAuthor("CubeReport", null, null))
                    .setDescription(String.format(":question: **%s** asks for help: *%s*",
                            CubeReportPlugin.escapeDiscordEmbedText(senderName),
                            CubeReportPlugin.escapeDiscordEmbedText(inputMessage)))
                    .setColor(0x01a9f4)
                    .setTimestamp(Instant.now());
            client.send(builder.build());
        }
    }
}
