/*

    cubereport
    Copyright (C) 2017  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.cubereport;

import club.minnced.discord.webhook.WebhookClient;
import club.minnced.discord.webhook.WebhookClientBuilder;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

public final class CubeReportPlugin extends Plugin {

    private Configuration config;
    private Configuration data;
    private WebhookClient client;
    private ScheduledExecutorService executor;
    // TODO(traks): consider removing entries from this map somehow. Currently
    // they're kept in here forever
    private final Map<UUID, Long> commandCooldown = new ConcurrentHashMap<>();

    @Override
    public void onEnable() {
        saveDefaultConfig("config.yml");
        saveDefaultConfig("data.yml");
        config = loadConfig("config.yml");
        data = loadConfig("data.yml");

        final String webhookString = getConfig().getString("webhook_url", "");
        if (!webhookString.isEmpty()) {
            getLogger().info("The Discord webhook is enabled");
            var builder = new WebhookClientBuilder(webhookString);
            executor = Executors.newSingleThreadScheduledExecutor();
            builder.setExecutorService(executor);
            client = builder.build();
            client.setErrorHandler((client, message, throwable) -> {
                getLogger().log(Level.SEVERE, message, throwable);
            });
        } else {
            getLogger().warning("The Discord webhook is disabled");
        }

        getProxy().getPluginManager().registerCommand(this, new ReportCommand(this));
        getProxy().getPluginManager().registerCommand(this, new HelpOpCommand(this));
        getProxy().getPluginManager().registerListener(this, new Events(this));
    }

    @Override
    public void onDisable() {
        getLogger().info("Saving data...");
        saveConfig("data.yml", data);

        if (client != null) {
            getLogger().info("Closing the Discord webhook...");
            client.close();
            try {
                executor.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
            } catch (InterruptedException e) {
                getLogger().log(Level.WARNING, "Couldn't wait for the webhook to close", e);
            }
        }

        getLogger().info("All done");
    }

    public Configuration getConfig() {
        return config;
    }

    public Configuration getData() {
        return data;
    }

    public static String escapeDiscordEmbedText(String text) {
        return text.replaceAll("([\\\\~*_`|>:])", "\\\\$1");
    }

    public WebhookClient getDiscordClient() {
        return client;
    }

    public int getAndUpdateRemainingCooldownSecs(CommandSender sender) {
        if (sender instanceof ProxiedPlayer player) {
            var uuid = player.getUniqueId();
            long cooldownEndMillis = commandCooldown.getOrDefault(uuid, 0L);
            long now = System.currentTimeMillis();
            if (cooldownEndMillis > now) {
                // NOTE(traks): round up so we don't return a cooldown of 0 if
                // less than 1 second remains
                return (int) ((cooldownEndMillis - now + 999) / 1000);
            }
            long nextCooldown = now + getConfig().getInt("cooldown") * 1000L;
            commandCooldown.put(uuid, nextCooldown);
        }
        return 0;
    }

    public void saveDefaultConfig(String fileName) {
        getDataFolder().mkdirs();
        var configFile = new File(getDataFolder(), fileName);
        try {
            Files.copy(getResourceAsStream(fileName), configFile.toPath());
        } catch (FileAlreadyExistsException e) {
            // ignore
        } catch (IOException e) {
            getLogger().log(Level.WARNING, "Can't save default config " + fileName, e);
        }
    }

    public Configuration loadConfig(String fileName) {
        var configProvider = ConfigurationProvider.getProvider(YamlConfiguration.class);
        var file = new File(getDataFolder(), fileName);
        try {
            return configProvider.load(file);
        } catch (IOException e) {
            if (file.exists()) {
                getLogger().log(Level.WARNING, "Can't load config " + fileName, e);
            }
            return new Configuration();
        }
    }

    public void saveConfig(String fileName, Configuration config) {
        getDataFolder().mkdirs();
        var configFile = new File(getDataFolder(), fileName);
        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(config, configFile);
        } catch (IOException e) {
            getLogger().log(Level.WARNING, "Can't save config " + fileName, e);
        }
    }
}
