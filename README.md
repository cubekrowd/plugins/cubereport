# CubeReport

CubeReport is an efficient and simple BungeeCord plugin that lets players contact staff. Players can report rulebreakers to staff with /report. Players can ask staff for help using /helpop. All reports and help requests can also be logged to a Discord channel. This plugin can also log BanManager punishments to Discord.

## Installation

Simply drop the plugin into your BungeeCord plugin folder and restart. Remember to edit the config and add the Discord webhook if you want the reports to be saved. To log kicks, enable logKicks in BanManager's configuration file.

## Commands

This section describes all commands this plugin provides.

| Command | Description
| ------- | -----------
| /report toggle | Hide or show player reports for yourself
| /report \<player\> \<message\> | Report a player
| /helpop \<message\> | Ask for help

## Permissions

Administrators can grant the following permissions to players:

| Permission | Description
| ---------- | -----------
| cubereport.report.use | Access to /report
| cubereport.report.see | See incoming reports
| cubereport.helpop.use | Access to /helpop
| cubereport.helpop.see | See incoming help requests

## Credits

This plugin is Open-Source, released under AGPL-3.0.
If you want to contribute you can find the repository at:  
[https://gitlab.com/cubekrowd/cubereport](https://gitlab.com/cubekrowd/cubereport "https://gitlab.com/cubekrowd/cubereport")
